import request from "supertest";
import assert from "assert";
import dotenv from "dotenv";

jest.mock("../src/middleware/auth", () => ({
    AuthMiddleware: jest.fn().mockImplementation((req, res, next) => next())
}));

dotenv.config({ path: ".env.test" });

import app from "../src/app";
import { dbConnect } from "../src/util/db1";
import { getManager } from "typeorm";
import { User } from "../src/models/user";
import { UserSeed } from "../src/seeds/user";
import { File } from "../src/models/file";
import { FileSeed } from "../src/seeds/file";

beforeAll(async () => {
    const connection = await dbConnect();
    await connection.runMigrations();
    await getManager().getRepository(User).query("Truncate table users");
    await getManager().getRepository(File).query("truncate table files");
    await getManager().getRepository(User).save(UserSeed);
    await getManager().getRepository(File).save(FileSeed);
});


describe("POST /api/login", () => {
    it("User login success", () => {
        return request(app)
            .post("/api/login")
            .send({"email": "admin@api.io", "password": "123"})
            .set("Accept", "application/json")
            .expect(201)
            .then(response => {
                expect(!!response.body.auth_token);
            });
    });
});

describe("POST /api/login", () => {
    it("User login failed", () => {
        return request(app)
            .post("/api/login")
            .send({"email": "admin@api.io", "password": "12"})
            .set("Accept", "application/json")
            .expect(401);
    });
});

describe("GET /api/search", () => {
    it("File search with all filters", async () => {
        return request(app)
            .get("/api/search?name=all&sort=desc&type=text/javascript")
            .set("Authorization", `Bearer test`)
            .expect(200)
            .then(response => {
                assert.equal(!!response.body.items, true);
                assert.equal(response.body.items.length, 1);
            });
    });

    it("File search with name filter", () => {
        return request(app)
            .get("/api/search?name=all")
            .set("Authorization", `Bearer test`)
            .expect(200)
            .then(response => {
                assert.equal(!!response.body.items, true);
                assert.equal(response.body.items.length, 3);
            });
    });
});

