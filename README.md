# How to run
```
$ docker-compose up -d
```

```
$ npm install
```

```
$ npm run build
```

```
$ npm start
```


# How to testing
- Create database api_test
```
$ docker-compose exec mysql sh
```
```
$ mysql -uroot -p123
```
```
$ create database api_test
```
- Run next command
```
$ npm run test
```
