import mime from "mime";
import { FileMessage } from "./queue";

export function processFilePath(path: string): FileMessage {
    path = path.replace(process.env.SCANNED_DIR + "/", "");
    const pathArray = path.split("/");
    const file: string = pathArray.pop();
    const type: string = mime.getType(path);
    return {name: file, path: path, nesting_level: pathArray.length, type: type};
}

export function isDeveloperEnv(): boolean {
    return process.env.ENV == "dev";
}

export function isTestEnv(): boolean {
    return process.env.ENV == "test";
}
