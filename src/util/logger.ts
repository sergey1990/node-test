import { Logger, LoggerInstance, LoggerOptions, transports } from "winston";
import appRoot from "app-root-path";


const options: LoggerOptions = {
    transports: [
        new transports.Console({
            level: process.env.NODE_ENV === "production" ? "error" : "debug",
            handleExceptions: true,
            json: false,
            colorize: true,
        }),
        new transports.File({
            level: "debug",
            filename: `${appRoot}/logs/app.log`,
            handleExceptions: true,
            json: true,
        })
    ]
};

const logger: LoggerInstance = new Logger(options);

const stream = {
    write: (message: any) => {
        logger.info(message);
    },
};

if (process.env.NODE_ENV !== "production") {
    logger.debug("Logging initialized at debug level");
}

export { logger, stream };

