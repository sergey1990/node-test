
export interface FileMessage {
    name: string;
    path: string;
    nesting_level: number;
    type: string;
}
