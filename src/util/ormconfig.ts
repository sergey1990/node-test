import { ConnectionOptions } from "typeorm";
import { File } from "../models/file";
import { User } from "../models/user";
import { MysqlConnectionOptions } from "typeorm/driver/mysql/MysqlConnectionOptions";

const config: MysqlConnectionOptions = {
    synchronize: false,
    migrationsRun: false,
    type: "mysql",
    host: process.env.MYSQL_HOST,
    port: parseInt(process.env.MYSQL_PORT),
    username: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE,
    logging: false,
    entities: [
        File,
        User
    ],
    migrations: [
        "./dist/migrations/*.js"
    ],
    cli: {
        migrationsDir: "./src/migrations",
    }
};

export = config;
