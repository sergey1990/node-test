import { createConnection } from "typeorm";
import config from "./ormconfig";

export function dbConnect() {
    return createConnection(config);
}
