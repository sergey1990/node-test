import { Request, Response, NextFunction } from "express";
import { promisify } from "util";
import jwt from "jsonwebtoken";
import { header, validationResult } from "express-validator/check";

const allowPaths = [
    "/api/login"
];

export async function AuthMiddleware(req: Request, res: Response, next: NextFunction) {

    if (allowPaths.includes(req.path)) {
        next();
        return;
    }

    try {
        header("authhorization", "Authorization header is invalid!").matches(/^Bearer\s.+$/);
        validationResult(req).throw();

        const token = req.headers.authorization.split(" ")[1];
        const decoded = await promisify(jwt.verify)(token, process.env.HASH_SECRET);

        // res.userId = decoded.id;
        next();

    } catch (e) {
        res.status(400).send({error: "Token is invalid"});
    }
}
