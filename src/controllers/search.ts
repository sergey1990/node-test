import { Request, Response } from "express";
import { service } from "../services/search";

/**
 *
 * @param req
 * @param res
 * @constructor
 */
export async function SearchAction(req: Request, res: Response) {
    req.assert("sort", "Sort param is not valid").optional().isIn(["asc", "desc"]);
    req.assert("name", "Name cannot be blank").notEmpty();
    req.assert("depth", "Depth should be numeric").optional().matches(/^\d+$/);

    const errors = req.validationErrors();

    if (errors) {
        res.status(400).send({errors: errors});
        return;
    }

    try {
        const files = await service.find(req.query);
        res.status(200).send({items: files});

    } catch (e) {
        console.log(e);
        res.status(500).send({error: "Error occurs on server"});
    }
}
