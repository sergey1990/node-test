import { Request, Response } from "express";
import { service } from "../services/auth";


/**
 * GET /login
 * Login page.
 */
export async function LoginAction(req: Request, res: Response) {
    req.assert("email", "Email is not valid").isEmail();
    req.assert("password", "Password cannot be blank").notEmpty();
    req.sanitize("email").normalizeEmail({gmail_remove_dots: false});

    const errors = req.validationErrors();

    if (errors) {
        res.status(400).send({errors: errors});
        return;
    }

    try {
        const token: string = await service.authorize({
            email: req.body.email,
            password: req.body.password
        });

        res.status(201).send({auth_token: token});

    } catch (e) {
        console.log(e);
        res.status(401).send({error: "Unauthorized"});
    }
}
