import { File } from "../models/file";
import { getManager } from "typeorm";

interface SearchFilterInterface {
    name: string;
    type?: string;
    depth?: number;
    sort?: string;
}

class SearchService {

    find(filter: SearchFilterInterface): Promise<File[]> {
        const replacements: SearchFilterInterface = {name: filter.name};
        let whereSql = "MATCH(name) AGAINST(:name)";
        let sort: string = "ASC";

        if (filter.type) {
            whereSql = whereSql + " AND type = :type";
            replacements.type = filter.type;
        }

        if (filter.depth) {
            whereSql = whereSql + " AND nesting_level = :depth";
            replacements.depth = filter.depth;
        }

        if (filter.sort  ) {
            sort = filter.sort;
        }

        return getManager()
            .getRepository(File)
            .createQueryBuilder()
            .where(whereSql, replacements)
            .orderBy("name", sort === "ASC" ? sort : "DESC")
            .getMany();
    }
}

const service = new SearchService();

export { service };


