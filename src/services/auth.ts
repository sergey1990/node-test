import { promisify } from "util";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { getManager } from "typeorm";
import { User } from "../models/user";

const sign: (arg: object, secret: Buffer) => Promise<string> = promisify(jwt.sign);

interface Credentials {
    email: string;
    password: string;
}

class AuthService {

    public async authorize(cred: Credentials): Promise<string> {
        const user = await getManager().getRepository(User).findOne({email: cred.email});
        if (user === undefined) throw new Error("");

        const isCompared: boolean = await bcrypt.compare(cred.password, user.password);
        if (!isCompared) throw new Error("");

        return sign({id: user.id}, Buffer.from(process.env.HASH_SECRET));
    }
}

const service = new AuthService();

export { service, Credentials };


