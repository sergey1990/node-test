import { ScannerWorker } from "./scanner/listener";
import { HandlerWorker } from "./handler/listener";
import { DirChangesHandlerWorker } from "./dirChangesHandler/listener";
import { WorkerListener } from "./listener.interface";

export enum WorkerType {
    Scanner,
    Listener,
    DirChangesHandler,
}

export function workerFactory(type: WorkerType): WorkerListener {
    switch (type) {
        case WorkerType.Scanner:
            return new ScannerWorker();
        case WorkerType.Listener:
            return new HandlerWorker();
        case WorkerType.DirChangesHandler:
            return new DirChangesHandlerWorker();
    }
}
