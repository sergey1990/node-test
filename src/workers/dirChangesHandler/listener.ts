import { WorkerListener } from "../listener.interface";
const {Worker} = require("worker_threads");
import { logger } from "../../util/logger";

export class DirChangesHandlerWorker implements WorkerListener {

    public onLine() {
        logger.debug("FileChangesHandler: started...");
    }

    public onMessage(message: string) {
        logger.debug("FileChangesHandler: " + message);
    }

    public onError(err: any) {
        logger.error("FileChangesHandler: Error");
        logger.error(err);
    }

    public onExit(code: any) {
        logger.debug("FileChangesHandler: finished...");
    }

    public run() {
        const worker = new Worker(__dirname + "/worker.js");
        worker.on("online", this.onLine);
        worker.on("message", this.onMessage);
        worker.on("error", this.onError);
        worker.on("exit", this.onExit);
    }
}
