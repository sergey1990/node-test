import * as chokidar from "chokidar";
// import { threadId, parentPort } from "worker_threads";
import { logger } from "../../util/logger";
import { processFilePath } from "../../util/helper";
import { FileMessage } from "../../util/queue";
import { Channel, connect, Connection } from "amqplib";

async function runDirChangesHandler() {
    const conn: Connection = await connect(process.env.RABBITMQ_URL);
    const channel: Channel = await conn.createChannel();

    chokidar
        .watch(process.env.SCANNED_DIR, {ignoreInitial: true, ignored: /(^|[\/\\])\../})
        .on("add", async (path) => {
            logger.debug(`DirChangesListener: New file (${path}) has been created`);
            const message: FileMessage = processFilePath(path);

            const ok = await channel.assertQueue(process.env.QUEUE_FILE_ADD, {durable: false});
            channel.sendToQueue(process.env.QUEUE_FILE_ADD, Buffer.from(JSON.stringify([message])));

        })
        .on("unlink", async (path) => {
            logger.debug(`DirChangesListener: New file (${path}) has been deleted`);

            const ok = await channel.assertQueue(process.env.QUEUE_FILE_REMOVE, {durable: false});
            channel.sendToQueue(process.env.QUEUE_FILE_REMOVE, Buffer.from(path));
        });
}

runDirChangesHandler();




