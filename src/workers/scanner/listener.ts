import { WorkerListener } from "../listener.interface";
const { Worker } = require("worker_threads");
import { logger } from "../../util/logger";


export class ScannerWorker implements WorkerListener {

    public cb: () => void;

    public onLine() {
        logger.debug("Scanner: started...");
    }

    public onMessage(message: string) {
        logger.debug("Scanner: " + message);
    }

    public onError(err: any) {
        logger.error("Scanner: Error");
        logger.error(err);
    }

    public onExit(code: any) {
        logger.debug("Scanner: finished...");
    }

    public doAfterFinish(cb: () => void) {
        this.cb = cb;
    }

    public run() {
        const worker = new Worker(__dirname + "/worker.js");
        worker.on("online", this.onLine);
        worker.on("message", this.onMessage);
        worker.on("error", this.onError);
        worker.on("exit", (code: any) => {
            this.onExit(code);
            this.cb();
        });
    }
}
