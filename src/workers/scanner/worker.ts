// import { threadId, parentPort } from "worker_threads";
import { promisify } from "util";
import * as fs from "fs";
import { logger } from "../../util/logger";
import { processFilePath } from "../../util/helper";
import { FileMessage } from "../../util/queue";
import { connect, Channel, Connection } from "amqplib";

const fsReaddir = promisify(fs.readdir);
const fsStat = promisify(fs.stat);

console.log(process.env.ENV);

async function runFullScan() {
    try {
        const conn: Connection = await connect(process.env.RABBITMQ_URL);
        const channel: Channel = await conn.createChannel();
        const message: FileMessage[] = [];

        function closeQueueConnection() {
            setTimeout(() => {
                conn.close();
            }, 1500);
        }

        async function scan(dir: string) {
            logger.debug(`Scanner: start scan dir ${dir}`);
            const files = await fsReaddir(dir);

            await Promise.all(files.map(async file => {
                const filepath = `${dir}/${file}`;
                const stat = await fsStat(filepath);

                if (stat.isFile()) {
                    message.push(processFilePath(filepath));

                    // Send data to queue when buffer is filled.
                    if (message.length > 50) {
                        const messageBuf: FileMessage[] = message.splice(0, 50);
                        const ok = await channel.assertQueue(process.env.QUEUE_FILE_ADD, {durable: false});
                        await channel.sendToQueue(process.env.QUEUE_FILE_ADD, Buffer.from(JSON.stringify(messageBuf)));
                    }

                    return Promise.resolve(true);

                } else if (stat.isDirectory()) {
                    return await scan(filepath);
                }

                return Promise.resolve(true);
            }));

            logger.debug(`Scanner: end scan dir ${dir}`);
            return Promise.resolve(true);
        }

        await scan(process.env.SCANNED_DIR);
        closeQueueConnection();
        if (message.length > 0) {
            await channel.assertQueue(process.env.QUEUE_FILE_ADD, {durable: false});
            await channel.sendToQueue(process.env.QUEUE_FILE_ADD, Buffer.from(JSON.stringify(message)));
        }

    } catch (e) {
        console.log(e);
    }
}

runFullScan();




