export interface WorkerListener {
    onLine(): void;

    onMessage(message: string): void;

    onError(err: any): void;

    onExit(code: any): void;

    run(): void;

    doAfterFinish?(cb: Function): void;
}
