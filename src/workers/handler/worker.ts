// import { threadId, parentPort } from "worker_threads";
import { Channel, connect, Connection, ConsumeMessage } from "amqplib";
import { logger } from "../../util/logger";
import { FileMessage } from "../../util/queue";
import { getManager } from "typeorm";
import { File } from "../../models/file";
import { dbConnect } from "../../util/db1";

async function listen() {
    try {
        await dbConnect();
        const conn: Connection = await connect(process.env.RABBITMQ_URL);
        const channel: Channel = await conn.createChannel();

        await channel.assertQueue(process.env.QUEUE_FILE_ADD, {durable: false});
        await channel.assertQueue(process.env.QUEUE_FILE_REMOVE, {durable: false});

        channel.consume(process.env.QUEUE_FILE_ADD, async (msg: ConsumeMessage) => {
            logger.debug("Listener: New files received");

            const items = JSON.parse(msg.content.toString());

            await channel.ack(msg);
            await getManager()
                .createQueryBuilder()
                .insert()
                .into(File)
                .values(items)
                .execute();
            logger.debug("Listener: New files were been added to storage");
        });

        channel.consume(process.env.QUEUE_FILE_REMOVE, async (msg: ConsumeMessage) => {
            logger.debug("Listener: New files received");

            const path = msg.content.toString();

            await channel.ack(msg);
            await getManager()
                .createQueryBuilder()
                .delete()
                .from(File)
                .where("path = :path", {path: path})
                .execute();
            logger.debug("Listener: New files were been removed from storage");
        });
    } catch (e) {
        logger.error(e);
    }
}

listen();




