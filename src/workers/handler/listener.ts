import { WorkerListener } from "../listener.interface";
const {Worker} = require("worker_threads");
import { logger } from "../../util/logger";

export class HandlerWorker implements WorkerListener {

    public onLine() {
        logger.debug("Handler: started...");
    }

    public onMessage(message: string) {
        logger.debug("Listener: " + message);
    }

    public onError(err: any) {
        logger.error("Handler: Error");
        logger.error(err);
    }

    public onExit(code: any) {
        logger.debug("Handler: finished...");
    }

    public run() {
        const worker = new Worker(__dirname + "/worker.js");
        worker.on("online", this.onLine);
        worker.on("message", this.onMessage);
        worker.on("error", this.onError);
        worker.on("exit", this.onExit);
    }
}
