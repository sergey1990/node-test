import { Entity, Column, PrimaryColumn } from "typeorm";

@Entity("users")
export class User {
    @PrimaryColumn()
    public  id: number;

    @Column()
    public email: string;

    @Column()
    public password: string;

    @Column()
    public first_name: string;
}

