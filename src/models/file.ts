import { Entity, Column, PrimaryColumn } from "typeorm";

@Entity("files")
export class File {

    @PrimaryColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    path: string;

    @Column()
    nesting_level: string;

    @Column()
    type: string;
    // public readonly created_at?: Date;
    // public readonly updated_at?: Date;
}
