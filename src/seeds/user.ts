import { User } from "../models/user";

const user1 = new User();
user1.email = "admin@api.io";
user1.first_name = "admin";
user1.password = "$2b$10$kVYsEc8bEBT4SyJkLzdG1Ot3W4oyz6iCpO2WqvKWlFmcorYoA2gFK";

export const UserSeed: User[] = [
    user1
];
