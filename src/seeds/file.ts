import { File } from "../models/file";

const file1 = new File();
file1.name = "allscripts.js";
file1.path = ".bin/allscripts.js";
file1.nesting_level = "2";
file1.type = "application/javascript";

const file2 = new File();
file2.name = "all.js";
file2.path = "ext/allscripts.js";
file2.nesting_level = "2";
file2.type = "text/javascript";

const file3 = new File();
file3.name = "allfiles.js";
file3.path = "lib/hello/allfiles.js";
file3.nesting_level = "3";
file3.type = "application/javascript";

const file4 = new File();
file4.name = "index.js";
file4.path = "lib/hello/index.js";
file4.nesting_level = "3";
file4.type = "application/javascript";

export const FileSeed: File[] = [
    file1, file2, file3, file4
];
