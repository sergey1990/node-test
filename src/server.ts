import errorHandler from "errorhandler";
import dotenv from "dotenv";

dotenv.config({ path: ".env" });

import app from "./app";
import { dbConnect } from "./util/db1";
import { isTestEnv } from "./util/helper";


/**
 * Error Handler. Provides full stack - remove for production
 */
app.use(errorHandler());

/**
 * Start Express server.
 */
const server = app.listen(app.get("port"), async () => {
  await dbConnect();
  console.log("  App is running at http://localhost:%d in %s mode",
    app.get("port"),
    app.get("env")
  );

  if (!isTestEnv()) {
    const { workerFactory, WorkerType } = require("./workers/worker.factory");
    const listener = workerFactory(WorkerType.Listener);
    listener.run();

    const dirChangesListener = workerFactory(WorkerType.DirChangesHandler);
    const scanner = workerFactory(WorkerType.Scanner);

    scanner.doAfterFinish(() => { dirChangesListener.run(); });
    scanner.run();
  }
});

export default server;
