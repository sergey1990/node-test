import express from "express";
import compression from "compression";
import bodyParser from "body-parser";
import expressValidator from "express-validator";
import { logger, stream } from "./util/logger";
import { LoginAction } from "./controllers/user";
import { SearchAction } from "./controllers/search";
import { AuthMiddleware } from "./middleware/auth";


const app = express();

app.set("port", process.env.PORT || 3000);
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(expressValidator());
app.use(require("morgan")("combined", {stream: stream}));
app.use(AuthMiddleware);
app.post("/api/login", LoginAction);
app.get("/api/search", SearchAction);

export default app;
